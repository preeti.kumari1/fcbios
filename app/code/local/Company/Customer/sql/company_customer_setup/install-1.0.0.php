<?php
$this->addAttribute('customer', 'company_name', array(
    'type'      => 'varchar',
    'label'     => 'Company Name',
    'input'     => 'text',
    'position'  => 120,
    'required'  => false,//or true
    'is_system' => 0,
));


$this->addAttribute('customer', 'company_address', array(
    'type'      => 'varchar',
    'label'     => 'Company Address',
    'input'     => 'text',
    'position'  => 121,
    'required'  => false,//or true
    'is_system' => 0,
));

$this->addAttribute('customer', 'office_phone', array(
    'type'      => 'varchar',
    'label'     => 'Phone Number Office',
    'input'     => 'text',
    'position'  => 123,
    'required'  => false,//or true
    'is_system' => 0,
));

$this->addAttribute('customer', 'phone_mobile', array(
    'type'      => 'varchar',
    'label'     => 'Phone Number Mobile',
    'input'     => 'text',
    'position'  => 124,
    'required'  => false,//or true
    'is_system' => 0,
));





$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'company_name');
$attribute->setData('used_in_forms', array(
    'adminhtml_customer',
    'checkout_register',
    'customer_account_create',
    'customer_account_edit',
));
$attribute->setData('is_user_defined', 0);
$attribute->save();

$attribute1 = Mage::getSingleton('eav/config')->getAttribute('customer', 'company_address');
$attribute1->setData('used_in_forms', array(
    'adminhtml_customer',
    'checkout_register',
    'customer_account_create',
    'customer_account_edit',
));
$attribute1->setData('is_user_defined', 0);
$attribute1->save();

$attribute2 = Mage::getSingleton('eav/config')->getAttribute('customer', 'office_phone');
$attribute2->setData('used_in_forms', array(
    'adminhtml_customer',
    'checkout_register',
    'customer_account_create',
    'customer_account_edit',
));
$attribute2->setData('is_user_defined', 0);
$attribute2->save();

$attribute3 = Mage::getSingleton('eav/config')->getAttribute('customer', 'phone_mobile');
$attribute3->setData('used_in_forms', array(
    'adminhtml_customer',
    'checkout_register',
    'customer_account_create',
    'customer_account_edit',
));
$attribute3->setData('is_user_defined', 0);
$attribute3->save();