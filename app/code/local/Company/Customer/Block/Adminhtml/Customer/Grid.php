<?php

class Company_Customer_Block_Adminhtml_Customer_Grid extends
    Mage_Adminhtml_Block_Customer_Grid
{
    public function setCollection($collection)
    {
        $collection->addAttributeToSelect('company_name')->addAttributeToSelect('company_address')->addAttributeToSelect('office_phone')->addAttributeToSelect('phone_mobile');
        $this->_collection = $collection;
    }

    protected function _prepareColumns()
    {
        $this->addColumnAfter('company_name', array(
            'header' => Mage::helper('customer')->__('Company'),
            'type' => 'text',
            'index' => 'company_name'
        ), 'billing_region');

        $this->addColumnAfter('company_address', array(
            'header' => Mage::helper('customer')->__('Company Address'),
            'type' => 'text',
            'index' => 'company_address',
        ), 'billing_region');
        
        $this->addColumnAfter('office_phone', array(
            'header' => Mage::helper('customer')->__('Phone Number (Office)'),
            'type' => 'text',
            'index' => 'office_phone',
        ), 'billing_region');
        
        $this->addColumnAfter('phone_mobile', array(
            'header' => Mage::helper('customer')->__('Phone Number (Mobile)'),
            'type' => 'text',
            'index' => 'phone_mobile',
        ), 'billing_region');

        return parent::_prepareColumns();
    }
}