jQuery(document).ready(function() {
 
  
	jQuery(window).on("load, resize", function() {
		var viewportWidth = jQuery(window).width();
		if (viewportWidth < 600) {
			jQuery(".right-side-menu").removeClass("right-side-menu").addClass("skip-links");
		}else{
			jQuery(".skip-links").removeClass("skip-links").addClass("right-side-menu");
		}
	});


	jQuery(".mbl_nav").click(function(){
	  jQuery(".mbl_navs").toggleClass("active");
	});



	jQuery(window).scroll(function(){
	  if (jQuery(window).scrollTop() >= 100) {
		jQuery('header').addClass('fixed-header');
	  }
	  else {
		jQuery('header').removeClass('fixed-header');
	  }
	});
   jQuery("#owl-demo-4").owlCarousel({
      loop:true,
      navigation : false, // Show next and prev buttons
      slideSpeed : 10,
      paginationSpeed : 100,
      singleItem:true,
      items : 5, 
      autoplay:true,
      autoplayTimeout:6000,
      responsiveClass:true,
      responsive:{
            0:{
            items:1,
            nav:false
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:5,
            nav:false,
            
        }
      }
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile :true
 
  }); 
  
  jQuery(".acheivement-inner ul li").click(function(){
/* jQuery(".year-desc").hide();
*/  jQuery(".acheivement-inner ul li").removeClass("active");
  jQuery(this).addClass("active");
  jQuery(this).parents().siblings().children('.year-desc').hide();

  
    var term = jQuery(this).attr('data-id');
    var finalID = '#' + term;
  jQuery(finalID).show();
 

});


if(jQuery(".acheivement-inner ul li").hasClass("active")){

    jQuery(this).parents().siblings().children('.year-desc').hide();
  }
  
  

  
  
  
});

